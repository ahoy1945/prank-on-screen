package com.prankonscreen.snakeonscreenfunnyjoke.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat.Builder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.RelativeLayout;

import com.prankonscreen.snakeonscreenfunnyjoke.R;
import com.prankonscreen.snakeonscreenfunnyjoke.mvp.home.HomeActivity;
import com.prankonscreen.snakeonscreenfunnyjoke.widget.FieldSurfaceView;

/**
 * Created by kodeartisan on 24/08/17.
 */

public class OverlayService extends Service {
    Editor editor;
    FieldSurfaceView fieldSurfaceView;
    Looper looper;
    SharedPreferences mPrefs;
    RelativeLayout rlay;
    private thread thread;

    private final class thread extends Handler {
        final OverlayService antOvealay;

        thread(OverlayService antOvealay, Looper looper) {
            super(looper);
            this.antOvealay = antOvealay;
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0) {
                this.antOvealay.Overlayout();
                this.antOvealay.Notification();
                return;
            }
            this.antOvealay.destroyLay();
        }
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void Notification() {
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.setFlags(872415232);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        if (this.mPrefs.getString("NotificationIcon", "").equals("Hide")) {
            startForeground(11, new Builder(this).setSmallIcon(R.mipmap.ic_launcher).setContentTitle(getResources().getString(R.string.app_name)).setContentText("STOP ME").setPriority(-2).setContentIntent(pendingIntent).build());
        } else {
            startForeground(11, new Builder(this).setSmallIcon(R.mipmap.ic_launcher).setContentTitle(getResources().getString(R.string.app_name)).setContentText("STOP ME").setContentIntent(pendingIntent).build());
        }
    }

    private void Overlayout() {
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 2006, 262408, -3);
        layoutParams.screenOrientation = 3;
        layoutParams.softInputMode = 48;
        this.rlay = (RelativeLayout) ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_overlay, null);
        wm.addView(this.rlay, layoutParams);
    }

    private synchronized void destroyLay() {
        if (this.rlay != null) {
            this.fieldSurfaceView = (FieldSurfaceView) this.rlay.findViewById(R.id.field);
            if (this.fieldSurfaceView != null) {
                this.fieldSurfaceView.set();
            }
            this.rlay.removeAllViews();
            try {
                ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(this.rlay);
            } catch (Exception e) {
                e.printStackTrace();
                this.rlay = null;
            }
            this.rlay = null;
        }
        stopForeground(true);
    }

    public void onCreate() {
        super.onCreate();
        this.mPrefs = getSharedPreferences("AntPreference", 0);
        this.editor = this.mPrefs.edit();
        this.editor.putString("Overlay", "true").apply();
        HandlerThread handlerThread = new HandlerThread("", 0);
        handlerThread.start();
        this.looper = handlerThread.getLooper();
        this.thread = new thread(this, this.looper);
    }

    public void onDestroy() {
        this.thread.sendEmptyMessage(1);
        if (this.rlay != null) {
            this.fieldSurfaceView = (FieldSurfaceView) this.rlay.findViewById(R.id.field);
            if (this.fieldSurfaceView != null) {
                this.fieldSurfaceView.set();
            }
        }
        this.editor.putString("Overlay", "false").apply();
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.thread.sendEmptyMessage(0);
        return START_STICKY;
    }
}

package com.prankonscreen.snakeonscreenfunnyjoke.constant;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by kodeartisan on 24/08/17.
 */

public class AppConstant {

    public static String IS_FIRST_TIME = "is_first_time";
    public static int FILECNT = 79;
    public static String FILENAME = "snake_";

    public interface  Preferences {
        String EDGE = "edge";
        String SPEED = "speed";
        String SOUND = "sound";
        String SIZE = "size";
        String DELAY = "delay";

        String DEFAULT_DELAY = "no_delay";
        String DEFAULT_SIZE = "small";
        String DEFAULT_SOUND = "on";
        String DEFAULT_SPEED = "very_fast";
        String DEFAULT_EDGE = "left_edge";

    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({LEFT_EDGE, RIGHT_EDGE, BOTTOM_EDGE, TOP_EDGE})
    public @interface EdgeMode{}
    public static final String LEFT_EDGE = "left_edge";
    public static final String RIGHT_EDGE = "right_edge";
    public static final String BOTTOM_EDGE = "bottom_edge";
    public static final String TOP_EDGE = "top_edge";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({SMALL, LARGE})
    public @interface SizeMode{}
    public static final String SMALL = "small";
    public static final String LARGE = "large";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({NO_DELAY, DELAY_10_SECOND, DELAY_30_SECOND, DELAY_60_SECOND, DELAY_120_SECOND})
    public @interface DelayMode{}
    public static final String NO_DELAY = "no_delay";
    public static final String DELAY_10_SECOND = "10_second";
    public static final String DELAY_30_SECOND = "30_second";
    public static final String DELAY_60_SECOND = "60_second";
    public static final String DELAY_120_SECOND = "120_second";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({SPEED_MODE_SLOW, SPEED_MODE_MEDIUM, SPEED_MODE_FAST})
    public @interface SpeedMode{}
    public static final String SPEED_MODE_SLOW = "slow";
    public static final String  SPEED_MODE_MEDIUM = "medium";
    public static final String  SPEED_MODE_FAST = "fast";
    public static final String  SPEED_MODE_VERY_FAST = "very_fast";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({SOUND_MODE_OFF, SOUND_MODE_ON})
    public @interface SoundMode{}
    public static final String SOUND_MODE_OFF = "off";
    public static final String SOUND_MODE_ON = "on";

}

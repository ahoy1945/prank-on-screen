package com.prankonscreen.snakeonscreenfunnyjoke.di.component;

import com.prankonscreen.snakeonscreenfunnyjoke.http.service.IfcfgService;
import com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.agent.IAgentRepository;
import com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.app.IAppRepository;
import com.prankonscreen.snakeonscreenfunnyjoke.di.module.ApplicationModule;
import com.prankonscreen.snakeonscreenfunnyjoke.di.module.DataModule;
import com.prankonscreen.snakeonscreenfunnyjoke.di.module.NetworkModule;
import com.prankonscreen.snakeonscreenfunnyjoke.rx.scheduler.BaseSchedulerProvider;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by kodeartisan on 23/08/17.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, DataModule.class})
public interface ApplicationComponent {

    BaseSchedulerProvider getSchedulerProvider();

    IAppRepository getIAppRepository();

    IAgentRepository getIAgentRepository();

    IfcfgService getIfcfgService();
}

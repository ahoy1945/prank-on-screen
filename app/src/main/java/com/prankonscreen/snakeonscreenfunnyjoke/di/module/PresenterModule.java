package com.prankonscreen.snakeonscreenfunnyjoke.di.module;

import android.support.annotation.NonNull;

import com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.app.IAppRepository;
import com.prankonscreen.snakeonscreenfunnyjoke.di.scope.ActivityScope;
import com.prankonscreen.snakeonscreenfunnyjoke.mvp.home.HomeContract;
import com.prankonscreen.snakeonscreenfunnyjoke.mvp.home.HomePresenter;
import com.prankonscreen.snakeonscreenfunnyjoke.rx.scheduler.BaseSchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kodeartisan on 24/08/17.
 */

@Module
public class PresenterModule {

    @ActivityScope
    @Provides
    HomeContract.Presenter homePresenter(@NonNull BaseSchedulerProvider schedulerProvider, @NonNull IAppRepository appRepository) {
        return new HomePresenter(schedulerProvider, appRepository);
    }
}

package com.prankonscreen.snakeonscreenfunnyjoke.di.module;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.prankonscreen.snakeonscreenfunnyjoke.di.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kodeartisan on 24/08/17.
 */
@Module
public class FragmentModule {

    private Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        this.mFragment = fragment;
    }

    @Provides
    @FragmentScope
    public Activity provideActivity() {
        return mFragment.getActivity();
    }
}

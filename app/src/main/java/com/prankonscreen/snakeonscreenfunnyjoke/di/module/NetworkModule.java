package com.prankonscreen.snakeonscreenfunnyjoke.di.module;

import android.support.annotation.NonNull;

import com.prankonscreen.snakeonscreenfunnyjoke.constant.Appkode4Constant;
import com.prankonscreen.snakeonscreenfunnyjoke.constant.IfcfgConstant;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.IfcfgService;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.appkode4.AgentService;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.appkode4.AppService;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.appkode4.ImageService;
import com.prankonscreen.snakeonscreenfunnyjoke.di.qualifier.RemoteAppkode4;
import com.prankonscreen.snakeonscreenfunnyjoke.di.qualifier.RemoteIfcfg;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kodeartisan on 23/08/17.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(@NonNull HttpLoggingInterceptor httpLoggingInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
               /* .connectTimeout(Constants.Api.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(Constants.Api.WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(Constants.Api.READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .cache(new Cache(context.getCacheDir(), Constants.Api.CACHE_SIZE))*/;

        return builder.build();
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor provideOkHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Singleton
    @Provides
    @RemoteAppkode4
    Retrofit provideRetrofitAppkode4(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(Appkode4Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Singleton
    @Provides
    @RemoteIfcfg
    Retrofit provideRetrofitIfcfg(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(IfcfgConstant.BASE_URL)

                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    IfcfgService provideIfcfgService(@NonNull @RemoteIfcfg Retrofit retrofit) {
        return retrofit.create(IfcfgService.class);
    }

    @Provides
    @Singleton
    AppService provideAppService(@NonNull @RemoteAppkode4 Retrofit retrofit) {
        return retrofit.create(AppService.class);
    }

    @Provides
    @Singleton
    AgentService provideAgentService(@NonNull @RemoteAppkode4 Retrofit retrofit) {
        return retrofit.create(AgentService.class);
    }

    @Provides
    @Singleton
    ImageService provideImageService(@NonNull @RemoteAppkode4 Retrofit retrofit) {
        return retrofit.create(ImageService.class);
    }
}

package com.prankonscreen.snakeonscreenfunnyjoke.di.module;

import android.app.Activity;

import com.prankonscreen.snakeonscreenfunnyjoke.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kodeartisan on 24/08/17.
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityScope
    public Activity proviceActivity() {
        return mActivity;
    }
}

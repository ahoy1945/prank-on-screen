package com.prankonscreen.snakeonscreenfunnyjoke.di.component;

import com.prankonscreen.snakeonscreenfunnyjoke.di.module.PresenterModule;
import com.prankonscreen.snakeonscreenfunnyjoke.di.scope.ActivityScope;
import com.prankonscreen.snakeonscreenfunnyjoke.mvp.home.HomeActivity;

import dagger.Component;

/**
 * Created by kodeartisan on 24/08/17.
 */
@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = PresenterModule.class)
public interface ActivityComponent {

    void inject(HomeActivity activity);
}

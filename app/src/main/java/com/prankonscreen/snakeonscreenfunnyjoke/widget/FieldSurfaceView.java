package com.prankonscreen.snakeonscreenfunnyjoke.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.media.SoundPool;
import android.media.SoundPool.Builder;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.prankonscreen.snakeonscreenfunnyjoke.R;
import com.prankonscreen.snakeonscreenfunnyjoke.constant.AppConstant;
import com.prankonscreen.snakeonscreenfunnyjoke.util.PreferencesUtil;

import java.util.Random;

/**
 * Created by kodeartisan on 24/08/17.
 */

public class FieldSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private float mSize = 1.0f;
    private int mSpeed = 0;
    boolean Change;

    Context context;
    int f11e;
    int edge;
    Bitmap f12f;
    private float fl;
    int f13j;
    Matrix matrix;
    Bitmap myBitmap;
    private long f14o;
    private Overthread overthread;
    private long f15p;
    Paint paint;
    Random random;
    Resources resources;

    boolean soundFlag;
    SoundPool soundPool;
    private int[] sounds;
    SurfaceHolder surfaceHolder;
    Bitmap temp;

    public class Overthread extends Thread {
        final FieldSurfaceView fieldSurfaceView;
        private SurfaceHolder mFieldSurfaceHolder;
        private boolean mRun;

        public Overthread(FieldSurfaceView fieldSurfaceView) {
            this.fieldSurfaceView = fieldSurfaceView;
            this.mFieldSurfaceHolder = fieldSurfaceView.getHolder();
        }

        public void setrun(boolean z) {
            this.mRun = z;
        }

        public void run() {
            super.run();
            while (this.mRun) {
                Canvas canvas = null;
                try {
                    sleep((long) FieldSurfaceView.this.mSpeed);
                    FieldSurfaceView fieldSurfaceView = FieldSurfaceView.this;
                    fieldSurfaceView.f11e++;
                    canvas = this.mFieldSurfaceHolder.lockCanvas();
                    this.fieldSurfaceView.drawing(canvas);
                    if (canvas != null) {
                        this.mFieldSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (canvas != null) {
                        this.mFieldSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Throwable th) {
                    if (canvas != null) {
                        this.mFieldSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    public FieldSurfaceView(Context context) {
        super(context);
        this.Change = false;
        this.f13j = 0;
        this.edge = 0;
        this.soundPool = null;
        this.random = new Random();
        this.soundFlag = false;
        this.fl = 0.0f;
        this.f14o = -1;
        this.f11e = -1;
        this.f12f = null;
        this.Change = false;
        set(context);
    }

    public FieldSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.Change = false;
        this.f13j = 0;
        this.edge = 0;
        this.soundPool = null;
        this.random = new Random();
        this.soundFlag = false;
        this.fl = 0.0f;
        this.f14o = -1;
        this.f11e = -1;
        this.f12f = null;
        this.Change = false;
        set(context);
    }

    private void set(Context context) {
        this.context = context;
        this.surfaceHolder = getHolder();
        this.surfaceHolder.addCallback(this);
        this.resources = context.getResources();
        this.setSize();
        this.setSpeed();
        this.setSound();

        //Log.e("msg", "" + this.AntSpeed + "  " + this.Speed);
        setZOrderOnTop(true);
        this.surfaceHolder.setFormat(-3);
        this.matrix = new Matrix();

    }

    public void drawing(Canvas canvas) {
        this.f12f = BitmapFactory.decodeResource(this.resources, this.resources.getIdentifier(AppConstant.FILENAME + this.f11e, "drawable", this.context.getPackageName()));
        canvas.drawColor(0, Mode.CLEAR);
        if (this.f11e >= AppConstant.FILECNT) {
            this.f11e = -1;
            if (this.Change) {
                this.Change = false;
            } else {
                this.Change = true;
            }
        }
        if (this.soundPool != null) {
            if (this.f11e == 121) {
                int i;
                this.soundFlag = this.random.nextInt(100) > 50;
                SoundPool soundPool = this.soundPool;
                int[] iArr = this.sounds;
                if (this.soundFlag) {
                    i = 0;
                } else {
                    i = 1;
                }
                soundPool.play(iArr[i], 1.0f, 1.0f, 1, 0, 1.0f);
            } else if (this.f11e == 202) {
                this.soundPool.play(this.sounds[this.soundFlag ? 1 : 0], 1.0f, 1.0f, 1, 0, 1.0f);
            }
        }
        float width = (float) getWidth();
        float height = (float) getHeight();
        float width2 = (float) this.f12f.getWidth();
        float height2 = (float) this.f12f.getHeight();
        this.matrix.reset();
        if (this.Change) {
            this.matrix.preScale(-1.0f, 1.0f, width2 / 2.0f, height2 / 2.0f);
        }
        float f = (height / width2) * this.mSize;
        float f2 = width - (f * height2);
        String edgeState = PreferencesUtil.getString(AppConstant.Preferences.EDGE, AppConstant.Preferences.DEFAULT_EDGE);

        switch (edgeState) {
            case AppConstant.LEFT_EDGE:
                this.matrix.postScale(f, f);
                this.matrix.postRotate(90.0f);
                this.matrix.postTranslate(width - f2, 0.0f);
                break;
            case AppConstant.RIGHT_EDGE:
                this.matrix.postScale(f, -f);
                this.matrix.postRotate(90.0f);
                this.matrix.postTranslate(width - (f * height2), 0.0f);
                break;
            case AppConstant.TOP_EDGE:
                width = (width / width2) * this.mSize;
                height = width * 1.5f;
                this.matrix.postScale(width, -height);
                this.matrix.postTranslate(0.0f, height * height2);
                break;

            case AppConstant.BOTTOM_EDGE:
                width = (width / width2) * this.mSize;
                f = width * 1.5f;
                this.matrix.postScale(width, f);
                this.matrix.postTranslate(0.0f, height - (f * height2));
                break;
        }
        canvas.drawBitmap(this.f12f, this.matrix, null);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.overthread = new Overthread(this);
        this.overthread.setrun(true);
        this.overthread.start();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.overthread.setrun(false);
        try {
            this.overthread.join();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        this.overthread = null;
        if (this.soundPool != null) {
            this.soundPool.release();
            this.soundPool = null;
        }
        Runtime.getRuntime().gc();
        Log.d("msgs..", "surface");
    }

    public void set() {
        if (this.overthread != null) {
            this.overthread.setrun(false);
        }
    }

    public void setSound() {
        String soundState = PreferencesUtil.getString(AppConstant.Preferences.SOUND, AppConstant.Preferences.DEFAULT_SOUND);
        if (soundState.equals(AppConstant.SOUND_MODE_ON)) {
            if (VERSION.SDK_INT >= 21) {
                this.soundPool = new Builder().setMaxStreams(2).build();
            } else {
                this.soundPool = new SoundPool(2, 3, 1);
            }
            this.sounds = new int[3];
            this.sounds[0] = this.soundPool.load(this.context, R.raw.sound1, 1);
            this.sounds[1] = this.soundPool.load(this.context, R.raw.sound2, 1);
        }
    }

    private void setSize() {
        String size = PreferencesUtil.getString(AppConstant.Preferences.SIZE, AppConstant.Preferences.DEFAULT_SIZE);
        switch (size) {
            case AppConstant.LARGE:
                this.mSize = 1.5f;
                break;
        }
    }

    private void setSpeed() {
        String speed = PreferencesUtil.getString(AppConstant.Preferences.SPEED, AppConstant.Preferences.DEFAULT_SPEED);
        switch (speed) {
            case AppConstant.SPEED_MODE_SLOW:
                this.mSpeed = 200;
                break;
            case AppConstant.SPEED_MODE_MEDIUM:
                this.mSpeed = 100;
                break;
            case AppConstant.SPEED_MODE_FAST:
                this.mSpeed = 50;
                break;
            case AppConstant.SPEED_MODE_VERY_FAST:
                this.mSpeed = 0;
                break;

        }
    }


}

package com.prankonscreen.snakeonscreenfunnyjoke.listener;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by kodeartisan on 20/09/17.
 */

public class InterstitialListener extends AdListener {

    private InterstitialAd mInterstitialAd;
    private Context mContext;

    public InterstitialListener(Context context, InterstitialAd interstitialAd) {
        this.mInterstitialAd = interstitialAd;
        this.mContext = context;
    }

    @Override
    public void onAdClosed() {
        super.onAdClosed();
    }

    @Override
    public void onAdFailedToLoad(int i) {
        super.onAdFailedToLoad(i);
        Toast.makeText(this.mContext, "onAdFailedToLoad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAdLeftApplication() {
        super.onAdLeftApplication();
    }

    @Override
    public void onAdOpened() {
        super.onAdOpened();
    }

    @Override
    public void onAdLoaded() {
        super.onAdLoaded();
        //Toast.makeText(this.mContext, "Adloaded", Toast.LENGTH_SHORT).show();
        mInterstitialAd.show();
    }

    @Override
    public void onAdClicked() {
        super.onAdClicked();
    }

    @Override
    public void onAdImpression() {
        super.onAdImpression();
    }
}

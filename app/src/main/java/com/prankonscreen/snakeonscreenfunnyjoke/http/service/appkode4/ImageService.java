package com.prankonscreen.snakeonscreenfunnyjoke.http.service.appkode4;


import com.prankonscreen.snakeonscreenfunnyjoke.data.wrapper.ImageWrapper;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by kodeartisan on 05/08/17.
 */

public interface ImageService {

    @GET("image")
    Observable<ImageWrapper> queryGet(@QueryMap Map<String, String> options);

}

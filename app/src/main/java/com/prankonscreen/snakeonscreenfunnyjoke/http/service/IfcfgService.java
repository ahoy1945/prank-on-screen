package com.prankonscreen.snakeonscreenfunnyjoke.http.service;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by kodeartisan on 24/08/17.
 */

public interface IfcfgService {
    @GET("/")
    Observable<Response<ResponseBody>> getPublicIp();
}

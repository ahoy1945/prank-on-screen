package com.prankonscreen.snakeonscreenfunnyjoke.rx.scheduler;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kodeartisan on 27/07/17.
 */

public class SchedulerProvider implements BaseSchedulerProvider {
    public static final String TAG = SchedulerProvider.class.getSimpleName();

    @NonNull
    @Override
    public Scheduler computation() {
        return Schedulers.computation();
    }

    @NonNull
    @Override
    public Scheduler multi() {
        return Schedulers.newThread();
    }

    @NonNull
    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @NonNull
    @Override
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}

package com.prankonscreen.snakeonscreenfunnyjoke.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.prankonscreen.snakeonscreenfunnyjoke.R;
import com.prankonscreen.snakeonscreenfunnyjoke.base.BaseAdapter;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;

import butterknife.BindView;

/**
 * Created by kodeartisan on 18/09/17.
 */

public class AppsAdapter extends BaseAdapter<App> {

    public AppsAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.adapter_apps, parent, false);

        return new AppsViewHolder(view);
    }

    public class AppsViewHolder extends BaseViewHolder {

        @BindView(R.id.imageMain)
        ImageView mImageMain;

        public AppsViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void fillView(int position) {

            Glide.with(mContext).load(mData.get(position).getIcon())
                    .into(mImageMain);
        }
    }
}

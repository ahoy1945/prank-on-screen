package com.prankonscreen.snakeonscreenfunnyjoke.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

/**
 * Created by kodeartisan on 26/08/17.
 */

public abstract class BaseActivityMvp<T extends BaseContract.BasePresenter> extends BaseActivity {

    @Inject
    protected T mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) mPresenter.detachView();
    }








}

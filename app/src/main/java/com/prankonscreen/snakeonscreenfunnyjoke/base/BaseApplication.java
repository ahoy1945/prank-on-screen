package com.prankonscreen.snakeonscreenfunnyjoke.base;

import android.app.Application;

import com.prankonscreen.snakeonscreenfunnyjoke.di.component.ApplicationComponent;
import com.prankonscreen.snakeonscreenfunnyjoke.di.component.DaggerApplicationComponent;
import com.prankonscreen.snakeonscreenfunnyjoke.di.module.ApplicationModule;
import com.prankonscreen.snakeonscreenfunnyjoke.util.Util;

/**
 * Created by kodeartisan on 23/08/17.
 */

public class BaseApplication extends Application {

    public static final String TAG = BaseApplication.class.getSimpleName();

    private static ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeComponent();
        Util.init(this);
    }

    private void initializeComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }



    public static ApplicationComponent getAppComponent() {
        return mApplicationComponent;
    }
}

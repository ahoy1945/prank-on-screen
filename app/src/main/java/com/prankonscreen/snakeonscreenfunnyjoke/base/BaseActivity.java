package com.prankonscreen.snakeonscreenfunnyjoke.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by kodeartisan on 23/08/17.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseContract.BaseView {

    public static final String TAG = BaseActivity.class.getSimpleName();

    protected Unbinder unbinder;

    protected ProgressDialog mProgressDialog;

    protected abstract @LayoutRes int getLayoutId();

    protected abstract void injectDependencies();

    protected abstract void initData(Bundle savedInstanceState);

    protected abstract void initActionBar();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
        initData(savedInstanceState);
        initActionBar();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(unbinder != null) unbinder.unbind();


    }

    @Override
    public void showOnError(String s) {
        if(mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }

    @Override
    public void showOnLoading() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle("Loading...");
        mProgressDialog.show();
    }


    @Override
    public void showOnSuccess() {
        if(mProgressDialog.isShowing()) mProgressDialog.dismiss();

    }
}

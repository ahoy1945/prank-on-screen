package com.prankonscreen.snakeonscreenfunnyjoke.base;

/**
 * Created by kodeartisan on 24/08/17.
 */

public interface BaseContract {

    interface BaseView {

        void showOnError(String s);

        void showOnLoading();

        void showOnSuccess();


    }

    interface BasePresenter<T>  {


        void attachView(T view);


        void detachView();
    }

}

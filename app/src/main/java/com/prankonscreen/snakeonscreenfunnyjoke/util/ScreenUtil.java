package com.prankonscreen.snakeonscreenfunnyjoke.util;

/**
 * Created by kodeartisan on 26/07/17.
 */

public final class ScreenUtil {

    public static final String TAG = ScreenUtil.class.getSimpleName();

    private ScreenUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static int getScreenWidth() {
        return Util.getContext().getResources().getDisplayMetrics().widthPixels;

    }

    public static int getScreenHeight() {
        return Util.getContext().getResources().getDisplayMetrics().heightPixels;
    }


}

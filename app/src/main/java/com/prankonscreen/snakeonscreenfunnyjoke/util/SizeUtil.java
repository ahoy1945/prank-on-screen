package com.prankonscreen.snakeonscreenfunnyjoke.util;

/**
 * Created by kodeartisan on 31/07/17.
 */

public final class SizeUtil {

    private SizeUtil() { }

    public static final long GB_2_BYTE = 1073741824;
    public static final long MB_2_BYTE = 1048576;
    public static final long KB_2_BYTE = 1024;
}

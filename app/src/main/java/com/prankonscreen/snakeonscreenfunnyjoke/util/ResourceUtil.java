package com.prankonscreen.snakeonscreenfunnyjoke.util;

/**
 * Created by kodeartisan on 02/08/17.
 */

public class ResourceUtil {

    private ResourceUtil() {

    }

    public static String getString(int id) {
        return Util.getContext().getResources().getString(id);
    }

    public static float getDimesion(int id) {
        return Util.getContext().getResources().getDimension(id);
    }
}

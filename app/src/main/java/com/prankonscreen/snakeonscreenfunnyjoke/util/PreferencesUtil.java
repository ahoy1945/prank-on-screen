package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kodeartisan on 31/07/17.
 */

public final class PreferencesUtil {

    public static final String TAG = PreferencesUtil.class.getSimpleName();

    private static final String SETTING = "setting";

    private static SharedPreferences sharedPreferences;

    private PreferencesUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    private static SharedPreferences getSharedPreferences() {
        if(sharedPreferences == null) {
            sharedPreferences = Util.getContext().getSharedPreferences(SETTING, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public static boolean contains(String key) {
        return getSharedPreferences().contains(key);
    }

    public static boolean getBoolean(final String key, final boolean defaultValue) {
        return getSharedPreferences().getBoolean(key, defaultValue);
    }

    public static int getInt(final String key, final int defaultValue) {
        return getSharedPreferences().getInt(key, defaultValue);
    }

    public static String getString(final String key, final String defaultValue) {
        return getSharedPreferences().getString(key, defaultValue);
    }

    public static boolean putBoolean(final String key, final boolean value) {
        final SharedPreferences.Editor editor = getSharedPreferences().edit();

        editor.putBoolean(key, value);

        return editor.commit();
    }

    public static boolean putString(final String key, final String value) {
        final SharedPreferences.Editor editor = getSharedPreferences().edit();

        editor.putString(key, value);

        return editor.commit();
    }

    public static boolean putInt(final String key, final int value) {
        final SharedPreferences.Editor editor = getSharedPreferences().edit();

        editor.putInt(key, value);

        return editor.commit();
    }
}

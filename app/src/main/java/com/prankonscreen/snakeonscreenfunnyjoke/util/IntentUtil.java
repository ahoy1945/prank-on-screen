package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.content.ActivityNotFoundException;
import android.content.Intent;

/**
 * Created by kodeartisan on 21/08/17.
 */

public class IntentUtil {

    public static final String TAG = IntentUtil.class.getSimpleName();

    public static void shareApp() {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out my app at: https://play.google.com/store/apps/details?id="+AppUtil.getAppPackageName());
            sendIntent.setType("text/plain");
            Util.getContext().startActivity(sendIntent);
        } catch (ActivityNotFoundException e) {
        }
    }

}

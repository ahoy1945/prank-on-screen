package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by kodeartisan on 17/09/17.
 */

public class LoadingUtil {

    public static final String TAG = LoadingUtil.class.getSimpleName();

    private LoadingUtil mInstance;

    private ProgressDialog mProgressDialog;

    public LoadingUtil Instance() {
        if(mInstance == null) {
            this.mInstance = new LoadingUtil();
        }

        return mInstance;
    }

    private void showLoading(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    private void dismissLoading() {
        if(mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }
}

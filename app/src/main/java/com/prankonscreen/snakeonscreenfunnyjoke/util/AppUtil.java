package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by kodeartisan on 26/07/17.
 */

public final class AppUtil {

    public static final String TAG = AppUtil.class.getSimpleName();

    private AppUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static String getAppPackageName() {
        return Util.getContext().getPackageName();
    }

    public static String getAppName() {
        return getAppName(Util.getContext().getPackageName());
    }

    public static String getAppName(final String packageName) {
        if (isSpace(packageName)) return null;
        try {
            PackageManager pm = Util.getContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packageName, 0);
            return pi == null ? null : pi.applicationInfo.loadLabel(pm).toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Drawable getAppIcon() {
        return getAppIcon(Util.getContext().getPackageName());
    }

    public static Drawable getAppIcon(final String packageName) {
        if (isSpace(packageName)) return null;
        try {
            PackageManager pm = Util.getContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packageName, 0);
            return pi == null ? null : pi.applicationInfo.loadIcon(pm);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }


    }

    public static String getAppPath() {
        return getAppPath(Util.getContext().getPackageName());
    }

    public static String getAppPath(final String packageName) {
        if (isSpace(packageName)) return null;
        try {
            PackageManager pm = Util.getContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packageName, 0);
            return pi == null ? null : pi.applicationInfo.sourceDir;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAppVersionName() {
        return getAppVersionName(Util.getContext().getPackageName());
    }

    public static String getAppVersionName(final String packageName) {
        if (isSpace(packageName)) return null;
        try {
            PackageManager pm = Util.getContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packageName, 0);
            return pi == null ? null : pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getAppVersionCode() {
        return getAppVersionCode(Util.getContext().getPackageName());
    }

    public static int getAppVersionCode(final String packageName) {
        if (isSpace(packageName)) return -1;
        try {
            PackageManager pm = Util.getContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packageName, 0);
            return pi == null ? -1 : pi.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public ArrayList<String> getTextStyle() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 1; i < 31; i++) {
            list.add("fonts/" + i + ".ttf");
        }
        return list;
    }

    public void addImageGallery(File file) {
        ContentValues values = new ContentValues();
        values.put("_data", file.getAbsolutePath());
        values.put("mime_type", "image/jpeg");
        Util.getContext().getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);
    }



    private static boolean isSpace(final String s) {
        if (s == null) return true;
        for (int i = 0, len = s.length(); i < len; ++i) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static void goToMarket(String packageName) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Util.getContext().startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            Intent intent = new Intent(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Util.getContext().startActivity(intent);
        }
    }

}

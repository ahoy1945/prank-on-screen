package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.os.Environment;
import android.os.StatFs;

import java.io.File;

/**
 * Created by kodeartisan on 31/07/17.
 */

public final class SDcardUtil {

    private SDcardUtil() { }

    public static boolean isCanUseSD() {
        try {
            return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static String checkDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getPath();
    }

    public static int freeSpaceOnSD() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double sdFreeMB =
                ((double) stat.getAvailableBlocks() * (double) stat.getBlockSize()) / 1024 * 1024;
        return (int) sdFreeMB;
    }
}

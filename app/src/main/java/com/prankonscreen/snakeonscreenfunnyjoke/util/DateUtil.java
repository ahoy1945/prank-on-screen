package com.prankonscreen.snakeonscreenfunnyjoke.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kodeartisan on 07/08/17.
 */

public class DateUtil {

    private DateUtil() {
    }

    private Calendar getCalendar() {
        return Calendar.getInstance();
    }

    private static SimpleDateFormat getSimpleDateFormat(String format) {
        return new SimpleDateFormat(format);
    }

    public static Date stringToDate(String date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date parsedDate = new Date();
        try {
            parsedDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parsedDate;
    }

    /*public static String DatetoString(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        
    }*/

}

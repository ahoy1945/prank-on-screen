package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Rect;
import android.support.v4.view.PointerIconCompat;

/**
 * Created by kodeartisan on 02/08/17.
 */

public final class ImageUtil {

    public static final String TAG = ImageUtil.class.getSimpleName();
    public static final double multiplier = 1.5d;

    private ImageUtil() {
    }

    public static Bitmap generateCircularBitmap(Bitmap input) {
        int width = input.getWidth();
        int height = input.getHeight();
        Bitmap outputBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Path path = new Path();
        path.addCircle((float) (width / 2), (float) (height / 2), (float) Math.min(width, height / 2), Path.Direction.CCW);
        Canvas canvas = new Canvas(outputBitmap);
        canvas.clipPath(path);
        canvas.drawBitmap(input, 0.0f, 0.0f, null);
        return outputBitmap;
    }

    public static Bitmap loadBitmap(String filename, WallpaperManager wallpaperManager) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);
        options.inSampleSize = calculateInSampleSize(options, wallpaperManager);
        options.inPreferQualityOverSpeed = true;
        return BitmapFactory.decodeFile(filename, options);
    }

    public static int calculateInSampleSize(Options options, WallpaperManager wallpaperManager) {
        int rawHeight = options.outHeight;
        int rawWidth = options.outWidth;
        int reqHeight = (int) (((double) wallpaperManager.getDesiredMinimumHeight()) * multiplier);
        int reqWidth = (int) (((double) wallpaperManager.getDesiredMinimumWidth()) * multiplier);
        int inSampleSize = 1;
        if (rawHeight > reqHeight || rawWidth > reqWidth) {
            inSampleSize = 2;
        }
        if (rawHeight > reqHeight * 2 || rawWidth > reqWidth * 2) {
            inSampleSize = 4;
        }

        return inSampleSize;
    }

    public static Bitmap scaleBitmap(Bitmap sampleBitmap, String setWallAspect, WallpaperManager wallpaperManager) {
        double heightBm = (double) sampleBitmap.getHeight();
        double widthBm = (double) sampleBitmap.getWidth();
        double heightDh = (double) wallpaperManager.getDesiredMinimumHeight();
        double widthDh = (double) wallpaperManager.getDesiredMinimumWidth();
        double width = 0.0d;
        double height = 0.0d;
        if (setWallAspect.equals("height")) {
            height = heightDh;
            width = (double) Math.round(widthBm * ((heightDh / heightBm) * 1.0d));
        } else if (setWallAspect.equals("width")) {
            width = widthDh;
            height = (double) Math.round(heightBm * ((widthDh / widthBm) * 1.0d));
        } else if (setWallAspect.equals("autofit")) {
            if (heightBm >= widthBm) {
                height = heightDh;
                width = (double) Math.round(widthBm * ((heightDh / heightBm) * 1.0d));
            } else {
                width = widthDh;
                height = (double) Math.round(heightBm * ((widthDh / widthBm) * 1.0d));
            }
        } else if (setWallAspect.equals("autofill")) {
            if (heightBm >= widthBm) {
                width = widthDh;
                height = (double) Math.round(heightBm * ((widthDh / widthBm) * 1.0d));
            } else {
                height = heightDh;
                width = (double) Math.round(widthBm * ((heightDh / heightBm) * 1.0d));
            }
        }
        sampleBitmap = Bitmap.createScaledBitmap(sampleBitmap, (int) width, (int) height, true);

        return sampleBitmap;
    }

    public static Bitmap prepareBitmap(Bitmap sampleBitmap, WallpaperManager wallpaperManager) {
        int heightBm = sampleBitmap.getHeight();
        int widthBm = sampleBitmap.getWidth();
        int heightDh = wallpaperManager.getDesiredMinimumHeight();
        int widthDh = wallpaperManager.getDesiredMinimumWidth();
        Bitmap changedBitmap;
        if (widthDh > widthBm || heightDh > heightBm) {
            int xPadding = Math.max(0, widthDh - widthBm) / 2;
            int yPadding = Math.max(0, heightDh - heightBm) / 2;
            changedBitmap = Bitmap.createBitmap(widthDh, heightDh, Bitmap.Config.ARGB_8888);
            int[] pixels = new int[(widthBm * heightBm)];
            sampleBitmap.getPixels(pixels, 0, widthBm, 0, 0, widthBm, heightBm);
            changedBitmap.setPixels(pixels, 0, widthBm, xPadding, yPadding, widthBm, heightBm);
            //Log.i(LOG_PROV, "WallpaperBitmaps Library: : Inflated size of Bitmap to fit device height/width in prepareBitmap");
            return changedBitmap;
        } else if (widthBm > widthDh || heightBm > heightDh) {
            changedBitmap = Bitmap.createBitmap(widthDh, heightDh, Bitmap.Config.ARGB_8888);
            Rect rect = new Rect(0, 0, widthDh, heightDh);
            Rect srcRect = new Rect();
            if (widthBm > widthDh) {
                rect = new Rect((widthBm - widthDh) / 2, 0, widthBm - ((widthBm - widthDh) / 2), heightBm);
            } else if (heightBm > heightDh) {
                rect = new Rect(0, (heightBm - heightDh) / 2, widthBm, heightBm - ((heightBm - heightDh) / 2));
            }
            new Canvas(changedBitmap).drawBitmap(sampleBitmap, srcRect, rect, null);
            //Log.i(LOG_PROV, "WallpaperBitmaps Library: Cropped size of Bitmap to fit device height/width in prepareBitmap");
            return changedBitmap;
        } else {
            changedBitmap = sampleBitmap;
            //Log.i(LOG_PROV, "WallpaperBitmaps Library: Did NOT inflate or crop Bitmap in prepareBitmap");
            return changedBitmap;
        }
    }

    public static Bitmap resizeBitmap(Bitmap bitmap) {
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        if (originalHeight > originalWidth) {
            newHeight = PointerIconCompat.TYPE_GRAB;
            newWidth = (int) (((float) newHeight) * (((float) originalWidth) / ((float) originalHeight)));
        } else if (originalWidth > originalHeight) {
            newWidth = PointerIconCompat.TYPE_GRAB;
            newHeight = (int) (((float) newWidth) * (((float) originalHeight) / ((float) originalWidth)));
        } else if (originalHeight == originalWidth) {
            newHeight = PointerIconCompat.TYPE_GRAB;
            newWidth = PointerIconCompat.TYPE_GRAB;
        }
        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / ((float) width);
        float scaleHeight = ((float) newHeight) / ((float) height);
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

}

package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by kodeartisan on 26/07/17.
 */

public final class Util {

    public static final String TAG = Util.class.getSimpleName();

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    private Util() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static void init(@NonNull final Context context) {
        Util.context = context.getApplicationContext();
    }

    public static Context getContext() {
        if (context != null) return context;
        throw new NullPointerException("u should init first");
    }
}
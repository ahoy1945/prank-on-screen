package com.prankonscreen.snakeonscreenfunnyjoke.util;

import android.annotation.SuppressLint;
import android.os.Build;
import android.provider.Settings;

import java.io.File;

/**
 * Created by kodeartisan on 26/07/17.
 */

public final class DeviceUtil {

    public static final String TAG = DeviceUtil.class.getSimpleName();

    private DeviceUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static boolean isDeviceRooted() {
        String su = "su";
        String[] locations = {"/system/bin/", "/system/xbin/", "/sbin/", "/system/sd/xbin/", "/system/bin/failsafe/",
                "/data/local/xbin/", "/data/local/bin/", "/data/local/"};
        for (String location : locations) {
            if (new File(location + su).exists()) {
                return true;
            }
        }
        return false;
    }

    public static int getSDKVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static String getSDKName() {
        return Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT].getName();
    }

    @SuppressLint("HardwareIds")
    public static String getAndroidID() {
        return Settings.Secure.getString(Util.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getModel() {
        String model = Build.MODEL;
        if (model != null) {
            model = model.trim().replaceAll("\\s*", "");
        } else {
            model = "";
        }
        return model;
    }

}

package com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.app;

import android.support.annotation.NonNull;


import com.prankonscreen.snakeonscreenfunnyjoke.constant.Appkode4Constant;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.Agent;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.IfcfgService;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.appkode4.AgentService;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.appkode4.AppService;
import com.prankonscreen.snakeonscreenfunnyjoke.data.wrapper.AgentWrapper;
import com.prankonscreen.snakeonscreenfunnyjoke.data.wrapper.AppListWrapper;
import com.prankonscreen.snakeonscreenfunnyjoke.data.wrapper.AppWrapper;
import com.prankonscreen.snakeonscreenfunnyjoke.rx.scheduler.BaseSchedulerProvider;
import com.prankonscreen.snakeonscreenfunnyjoke.util.DeviceUtil;
import com.prankonscreen.snakeonscreenfunnyjoke.util.NetworkUtil;
import com.prankonscreen.snakeonscreenfunnyjoke.util.ScreenUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by kodeartisan on 29/07/17.
 */
@Singleton
public class RemoteAppImpl implements IAppRepository {

    public static  final String TAG = RemoteAppImpl.class.getSimpleName();

    private final AppService appService;
    private final IfcfgService mIfcfgService;
    private final AgentService agentService;
    private final BaseSchedulerProvider schedulerProvider;

    @Inject
    public RemoteAppImpl(
            @NonNull  AppService appService,
            @NonNull IfcfgService ifcfgService,
            @NonNull AgentService agentService,
            @NonNull  BaseSchedulerProvider schedulerProvider ) {
        this.schedulerProvider = schedulerProvider;
        this.appService = appService;
        this.agentService = agentService;
        this.mIfcfgService = ifcfgService;

    }

    @Override
    public Observable<App> get(String id) {

        Observable<App> appObservable = appService.queryAppDetail(id)
                .subscribeOn(schedulerProvider.multi())
                .map(AppWrapper::getApp);


        Observable<Agent> agentObservable = mIfcfgService.getPublicIp()
                .subscribeOn(schedulerProvider.multi())
                .switchMap(response -> {
                    String ip = response.body().string();

                    return agentService.create(initAgent(ip));
                })
                .map(AgentWrapper::getAgent);


        return Observable.zip(appObservable,agentObservable,
                (App app, Agent agent) -> {
                    App newApp = new App();
                    newApp.setName(app.getName());
                    newApp.setPromoted(app.getPromoted());
                    return newApp;
                });

    }

    @Override
    public Observable<List<App>> getAppList(Map<String, String> options) {

        return appService.queryAppList(options)
                .subscribeOn(schedulerProvider.multi())
                .map(AppListWrapper::getAppList);
    }

    private Map<String, String> queryCategoryBy(String string) {
        Map<String, String> map = new HashMap<>();
        map.put("category", string);

        return map;
    }

    private Agent initAgent(String ipAddress) {
        Agent agent = new Agent();
        agent.setAppKey(Appkode4Constant.APP_ID);
        agent.setIpAdress(ipAddress);
        agent.setOsName(DeviceUtil.getSDKName());
        agent.setOsApiVersion(String.valueOf(DeviceUtil.getSDKVersion()));
        agent.setPhoneManufacturer(DeviceUtil.getManufacturer());
        agent.setPhoneModel(DeviceUtil.getModel());
        agent.setPhoneScreenSize(ScreenUtil.getScreenWidth() + "x" + ScreenUtil.getScreenHeight());
        agent.setNetworkCountryIso(NetworkUtil.getNetworkCountryISO());
        agent.setNetworkOperatorName(NetworkUtil.getNetworkOperatorName());
        agent.setNetworkType(NetworkUtil.getNetworkType().toString());
        agent.setRooted(DeviceUtil.isDeviceRooted());
        return agent;
    }

}

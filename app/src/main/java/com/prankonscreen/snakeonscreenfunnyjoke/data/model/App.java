package com.prankonscreen.snakeonscreenfunnyjoke.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kodeartisan on 29/07/17.
 */

public class App {

    private String id;
    private String name;
    private Boolean isPromoted;
    @SerializedName("package")
    private String packageName;
    private String icon;
    private List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Boolean getPromoted() {
        return isPromoted;
    }

    public void setPromoted(Boolean promoted) {
        isPromoted = promoted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}

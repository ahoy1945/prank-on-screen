package com.prankonscreen.snakeonscreenfunnyjoke.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kodeartisan on 28/07/17.
 */

public class Image{

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("url_link")
    private String urlLink;

    @SerializedName("category")
    private String category;

    @SerializedName("host")
    private String host;

    private String localUrlLink;

    public void setId(int id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getLocalUrlLink() {
        return localUrlLink;
    }

    public void setLocalUrlLink(String localUrlLink) {
        this.localUrlLink = localUrlLink;
    }
}

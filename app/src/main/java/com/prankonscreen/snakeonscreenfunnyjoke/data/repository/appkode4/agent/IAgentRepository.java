package com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.agent;


import com.prankonscreen.snakeonscreenfunnyjoke.data.model.Agent;

import io.reactivex.Observable;

/**
 * Created by kodeartisan on 30/07/17.
 */

public interface IAgentRepository {

    Observable<Agent> create(Agent agent);

}

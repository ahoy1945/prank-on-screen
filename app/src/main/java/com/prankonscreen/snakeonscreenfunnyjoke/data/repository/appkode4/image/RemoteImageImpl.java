package com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.image;

import android.support.annotation.NonNull;


import com.prankonscreen.snakeonscreenfunnyjoke.data.model.Image;
import com.prankonscreen.snakeonscreenfunnyjoke.http.service.appkode4.ImageService;
import com.prankonscreen.snakeonscreenfunnyjoke.data.wrapper.ImageWrapper;
import com.prankonscreen.snakeonscreenfunnyjoke.rx.scheduler.BaseSchedulerProvider;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by kodeartisan on 06/08/17.
 */
@Singleton
public class RemoteImageImpl implements IImageRepository {

    public static final String TAG = RemoteImageImpl.class.getSimpleName();

    private final ImageService imageService;
    private final BaseSchedulerProvider schedulerProvider;

    @Inject
    public RemoteImageImpl(@NonNull ImageService imageService,
                           @NonNull BaseSchedulerProvider schedulerProvider) {
        this.imageService = imageService;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public Observable<List<Image>> get(Map<String, String> options) {

        Observable<List<Image>> imageObservable = imageService.queryGet(options)
                .observeOn(schedulerProvider.multi())
                .map(ImageWrapper::getImages);

        return imageObservable;

    }
}

package com.prankonscreen.snakeonscreenfunnyjoke.data.wrapper;

import com.google.gson.annotations.SerializedName;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;

/**
 * Created by kodeartisan on 29/07/17.
 */

public class AppWrapper {

    @SerializedName("data")
    private App app;

    public void setApp(App app) {
        this.app = app;
    }

    public App getApp() {
        App app = new App();
        app.setName(this.app.getName());
        app.setPromoted(this.app.getPromoted());

        return app;
    }
}

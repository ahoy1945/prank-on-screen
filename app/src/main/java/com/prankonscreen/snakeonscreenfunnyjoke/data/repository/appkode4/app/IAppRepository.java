package com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.app;


import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by kodeartisan on 29/07/17.
 */

public interface IAppRepository {

    Observable<App> get(String id);
    Observable<List<App>> getAppList(Map<String, String> options);
}

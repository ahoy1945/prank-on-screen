package com.prankonscreen.snakeonscreenfunnyjoke.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kodeartisan on 30/07/17.
 */

public class Agent {

    private int id;

    @SerializedName("os_name")
    private String osName;

    @SerializedName("app_key")
    private String appKey;

    @SerializedName("ip_address")
    private String ipAdress;

    @SerializedName("os_api_version")
    private String osApiVersion;

    @SerializedName("phone_manufacturer")
    private String phoneManufacturer;

    @SerializedName("phone_model")
    private String phoneModel;

    @SerializedName("phone_screen_size")
    private String phoneScreenSize;

    @SerializedName("network_operator_name")
    private String networkOperatorName;

    @SerializedName("network_country_iso")
    private String networkCountryIso;

    @SerializedName("network_type")
    private String networkType;

    @SerializedName("is_rooted")
    private Boolean isRooted;

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getOsApiVersion() {
        return osApiVersion;
    }

    public void setOsApiVersion(String osApiVersion) {
        this.osApiVersion = osApiVersion;
    }

    public String getPhoneManufacturer() {
        return phoneManufacturer;
    }

    public void setPhoneManufacturer(String phoneManufacturer) {
        this.phoneManufacturer = phoneManufacturer;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getPhoneScreenSize() {
        return phoneScreenSize;
    }

    public void setPhoneScreenSize(String phoneScreenSize) {
        this.phoneScreenSize = phoneScreenSize;
    }

    public String getNetworkOperatorName() {
        return networkOperatorName;
    }

    public void setNetworkOperatorName(String networkOperatorName) {
        this.networkOperatorName = networkOperatorName;
    }

    public String getNetworkCountryIso() {
        return networkCountryIso;
    }

    public void setNetworkCountryIso(String networkCountryIso) {
        this.networkCountryIso = networkCountryIso;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public Boolean getRooted() {
        return isRooted;
    }

    public void setRooted(Boolean rooted) {
        isRooted = rooted;
    }

    public String getIpAdress() {
        return ipAdress;
    }

    public void setIpAdress(String ipAdress) {
        this.ipAdress = ipAdress;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
}

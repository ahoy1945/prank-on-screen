package com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.image;


import com.prankonscreen.snakeonscreenfunnyjoke.data.model.Image;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by kodeartisan on 06/08/17.
 */

public interface IImageRepository {

    Observable<List<Image>> get(Map<String, String> options);
}

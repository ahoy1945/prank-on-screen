package com.prankonscreen.snakeonscreenfunnyjoke.data.wrapper;

import com.google.gson.annotations.SerializedName;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;

import java.util.List;

/**
 * Created by kodeartisan on 15/08/17.
 */

public class AppListWrapper {

    @SerializedName("data")
    private List<App> appList;

    public AppListWrapper(List<App> appList) {
        this.appList = appList;
    }

    public List<App> getAppList() {
        return appList;
    }

    public void setAppList(List<App> appList) {
        this.appList = appList;
    }
}

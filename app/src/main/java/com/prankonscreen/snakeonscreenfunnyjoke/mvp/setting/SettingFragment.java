package com.prankonscreen.snakeonscreenfunnyjoke.mvp.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.prankonscreen.snakeonscreenfunnyjoke.R;
import com.prankonscreen.snakeonscreenfunnyjoke.constant.AppConstant;
import com.prankonscreen.snakeonscreenfunnyjoke.util.PreferencesUtil;

/**
 * Created by kodeartisan on 27/08/17.
 */

public class SettingFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG = SettingFragment.class.getSimpleName();

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.app_preferences);
        //PreferenceManager.setDefaultValues(getActivity(),R.xml.app_preferences, false);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case AppConstant.Preferences.DELAY:
                PreferencesUtil.putString(key, sharedPreferences.getString(key, AppConstant.Preferences.DEFAULT_DELAY));
                break;
            case AppConstant.Preferences.SIZE:
                PreferencesUtil.putString(key, sharedPreferences.getString(key, AppConstant.Preferences.DEFAULT_SIZE));
                break;
            case AppConstant.Preferences.SOUND:
                PreferencesUtil.putString(key, sharedPreferences.getString(key, AppConstant.Preferences.DEFAULT_SOUND));
                break;
            case AppConstant.Preferences.SPEED:
                PreferencesUtil.putString(key, sharedPreferences.getString(key, AppConstant.Preferences.DEFAULT_SPEED));
            case AppConstant.Preferences.EDGE:
                PreferencesUtil.putString(key, sharedPreferences.getString(key, AppConstant.Preferences.DEFAULT_EDGE));
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }
}

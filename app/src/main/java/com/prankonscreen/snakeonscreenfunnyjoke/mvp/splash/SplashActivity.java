package com.prankonscreen.snakeonscreenfunnyjoke.mvp.splash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.prankonscreen.snakeonscreenfunnyjoke.R;

public class SplashActivity extends AppCompatActivity {

    public static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}

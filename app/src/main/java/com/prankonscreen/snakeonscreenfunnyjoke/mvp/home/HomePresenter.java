package com.prankonscreen.snakeonscreenfunnyjoke.mvp.home;


import android.support.annotation.NonNull;

import com.prankonscreen.snakeonscreenfunnyjoke.constant.AppConstant;
import com.prankonscreen.snakeonscreenfunnyjoke.constant.Appkode4Constant;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;
import com.prankonscreen.snakeonscreenfunnyjoke.data.repository.appkode4.app.IAppRepository;
import com.prankonscreen.snakeonscreenfunnyjoke.rx.RxPresenter;
import com.prankonscreen.snakeonscreenfunnyjoke.rx.scheduler.BaseSchedulerProvider;
import com.prankonscreen.snakeonscreenfunnyjoke.util.LogUtil;
import com.prankonscreen.snakeonscreenfunnyjoke.util.PreferencesUtil;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by kodeartisan on 24/08/17.
 */

public class HomePresenter extends RxPresenter<HomeContract.View> implements HomeContract.Presenter<HomeContract.View> {

    public static final String TAG = HomePresenter.class.getSimpleName();

    private BaseSchedulerProvider mSchedulerProvider;
    private IAppRepository mAppRepository;
    private HomeContract.View mView;


    @Inject
    public HomePresenter(@NonNull BaseSchedulerProvider schedulerProvider, @NonNull IAppRepository appRepository) {
        this.mAppRepository = appRepository;
        this.mSchedulerProvider = schedulerProvider;

    }

    @Override
    public void initializeData() {
        this.mView.showOnLoading();

        addSubscribe(mAppRepository.get(Appkode4Constant.APP_ID)
                .subscribe(this::processData, this::processError));

    }

    @Override
    public void getAppList(Map<String, String> options) {
        addSubscribe(mAppRepository.getAppList(options)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(this::processDataAppList, this::processError));
    }

    private void processDataAppList(List<App> apps) {
        mView.onGetAppList(apps);
    }

    private void processError(Throwable throwable) {

        LogUtil.d(TAG, throwable.getMessage());
        this.mView.showOnError("Error");
    }

    private void processData(App app) {
        PreferencesUtil.putBoolean(AppConstant.IS_FIRST_TIME, false);
        this.mView.showOnSuccess();
    }

    @Override
    public void attachView(@NonNull HomeContract.View view) {
        super.attachView(view);
        this.mView = view;
    }
}

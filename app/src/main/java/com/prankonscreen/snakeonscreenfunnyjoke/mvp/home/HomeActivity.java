package com.prankonscreen.snakeonscreenfunnyjoke.mvp.home;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;


import com.google.android.gms.ads.InterstitialAd;
import com.prankonscreen.snakeonscreenfunnyjoke.R;
import com.prankonscreen.snakeonscreenfunnyjoke.adapter.AppsAdapter;
import com.prankonscreen.snakeonscreenfunnyjoke.base.BaseActivityMvp;
import com.prankonscreen.snakeonscreenfunnyjoke.base.BaseApplication;
import com.prankonscreen.snakeonscreenfunnyjoke.constant.AppConstant;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;
import com.prankonscreen.snakeonscreenfunnyjoke.di.component.DaggerActivityComponent;
import com.prankonscreen.snakeonscreenfunnyjoke.listener.InterstitialListener;
import com.prankonscreen.snakeonscreenfunnyjoke.mvp.setting.SettingActivity;
import com.prankonscreen.snakeonscreenfunnyjoke.receiver.OverlayReceiver;
import com.prankonscreen.snakeonscreenfunnyjoke.service.OverlayService;
import com.prankonscreen.snakeonscreenfunnyjoke.util.AdmobUtil;
import com.prankonscreen.snakeonscreenfunnyjoke.util.AppUtil;
import com.prankonscreen.snakeonscreenfunnyjoke.util.LogUtil;
import com.prankonscreen.snakeonscreenfunnyjoke.util.PreferencesUtil;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


public class HomeActivity extends BaseActivityMvp<HomeContract.Presenter> implements HomeContract.View, AppsAdapter.ItemClickListener {

    public static final String TAG = HomeActivity.class.getSimpleName();

    private StartAppAd startAppAd = new StartAppAd(this);
    private List<App> appList = new ArrayList<>();
    private AppsAdapter mAppsAdapter;
    private InterstitialAd mInterstitialAd;

    @BindView(R.id.recyclerview_app)
    RecyclerView mRecyclerviewApp;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void injectDependencies() {
        DaggerActivityComponent.builder()
                .applicationComponent(BaseApplication.getAppComponent())
                .build().inject(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
       StartAppSDK.init(this, getString(R.string.startapp_key), true);
        //AdmobUtil.s
        mInterstitialAd = AdmobUtil.loadInterstitial(new InterstitialAd(this), this);
        mPresenter.attachView(this);
        if(PreferencesUtil.getBoolean(AppConstant.IS_FIRST_TIME, true)) {
            mPresenter.initializeData();
        }
        getAppList();
        initAppsRecyclerView();


    }

    @Override
    protected void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setTitle("");
        }
    }


    @OnClick({R.id.btn_start, R.id.btn_setting, R.id.btn_rate_us, R.id.btn_share})
    public void OnClickedButton(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
               // showInterstitial();

                showPrank();
                break;
            case R.id.btn_setting:
               // showInterstitial();
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.btn_rate_us:
                Toast.makeText(this, "Rate us Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_share:
                Toast.makeText(this, "Share Us clicked", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void showPrank() {
        String delayState = PreferencesUtil.getString(AppConstant.Preferences.DELAY, AppConstant.Preferences.DEFAULT_DELAY);

        if(delayState.equals(AppConstant.NO_DELAY)) {
            startService(new Intent(this, OverlayService.class));
        } else {
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            PendingIntent pAlarmStopIntent = PendingIntent.getBroadcast(this, 3000000, new Intent(this, OverlayReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pAlarmStopIntent);
            alarmManager.set(0, System.currentTimeMillis() + this.getTime(), pAlarmStopIntent);

        }

        ActivityCompat.finishAffinity(this);
    }

    private long getTime() {
        String delayState = PreferencesUtil.getString(AppConstant.Preferences.DELAY, AppConstant.Preferences.DEFAULT_DELAY);
        long time = 0;
        switch (delayState) {
            case AppConstant.DELAY_10_SECOND:
                time = 10 * 1000;
                break;
            case AppConstant.DELAY_30_SECOND:
                time = 30 * 1000;
                break;
            case AppConstant.DELAY_60_SECOND:
                time = 60 * 1000;
                break;
            case AppConstant.DELAY_120_SECOND:
                time = 120 * 1000;
                break;
        }

        return time;
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopService(new Intent(this, OverlayService.class));
    }

    private void getAppList() {
        Map<String, String> options = new HashMap<>();
        options.put("category", "prank_on_screen");
        options.put("promoted", "true");
        options.put("limit", "6");
        options.put("random", "true");
        mPresenter.getAppList(options);
    }


    @Override
    public void onGetAppList(List<App> apps) {
        LogUtil.d(TAG, apps.size());
        appList = apps;
        mAppsAdapter.setData(apps);
    }

    private void initAppsRecyclerView() {
        mAppsAdapter = new AppsAdapter(this);
        mAppsAdapter.setItemClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        mRecyclerviewApp.setLayoutManager(linearLayoutManager);
        mRecyclerviewApp.setHasFixedSize(true);
        mRecyclerviewApp.setAdapter(mAppsAdapter);

    }

    @Override
    public void onItemClick(int position) {
        AppUtil.goToMarket(appList.get(position).getPackageName());
    }

    private void showInterstitial() {
        if(mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            mInterstitialAd = AdmobUtil.loadInterstitial(new InterstitialAd(this), this);
            mInterstitialAd.setAdListener(new InterstitialListener(this, mInterstitialAd));
        }
    }


}

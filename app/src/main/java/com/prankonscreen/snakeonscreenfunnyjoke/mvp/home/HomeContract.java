package com.prankonscreen.snakeonscreenfunnyjoke.mvp.home;

import com.prankonscreen.snakeonscreenfunnyjoke.base.BaseContract;
import com.prankonscreen.snakeonscreenfunnyjoke.data.model.App;

import java.util.List;
import java.util.Map;

/**
 * Created by kodeartisan on 24/08/17.
 */

public interface HomeContract {

    interface View extends BaseContract.BaseView {
        void onGetAppList(List<App> apps);

    }

    interface Presenter<T> extends BaseContract.BasePresenter<T> {
        void initializeData();
        void getAppList(Map<String, String> options);
    }
}

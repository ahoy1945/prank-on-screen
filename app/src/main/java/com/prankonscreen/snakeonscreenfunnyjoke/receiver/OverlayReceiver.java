package com.prankonscreen.snakeonscreenfunnyjoke.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.prankonscreen.snakeonscreenfunnyjoke.service.OverlayService;

/**
 * Created by kodeartisan on 25/08/17.
 */

public class OverlayReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context, "onReceive", Toast.LENGTH_SHORT).show();
        context.startService(new Intent(context.getApplicationContext(), OverlayService.class));
    }
}